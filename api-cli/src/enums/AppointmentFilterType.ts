export enum AppointmentFilterType {
    Service = 'service',
    Schedule = 'schedule'
}