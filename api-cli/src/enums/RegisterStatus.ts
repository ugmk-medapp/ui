export enum RegisterStatus {
    Success = 1,
    Exists = 2,
    ServerError = 3
}