import { AppointmentFilterType } from '../../enums/AppointmentFilterType';
import { ServiceType } from './ServiceType';

export interface GetAppointmentsRequest {
    medDepId: string;
    page: number;
    dateFilter?: string;
    searchName?: string;
    appTypeId: number;
    externalMedDepId: string;
    type: AppointmentFilterType;
    serviceType: ServiceType;
}
