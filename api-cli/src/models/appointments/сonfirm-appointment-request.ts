interface ConfirmAppointmentBase {
    medDepId: string;
    externalMedDepId: string;
    appointmentId: number;
}

export interface ConfirmLfkAppointmentRequest extends ConfirmAppointmentBase {
    complaints: string;
    program: string;
    progression: string;
    APbefore: number;
    APafter: number;
}

export interface ConfirmCheckupAppointmentRequest extends ConfirmAppointmentBase {
    complaints?: string;
    diagnosisId?: string;
    outcomeId?: string;
    resultId?: string;
    diseaseHistory?: string;
    objectively?: string;
    recommendations?: string;
    isClose?: boolean;
    sendToLK?: boolean;
    expertAnamnesis?: string;
}

export type ConfirmAppointmentRequest = ConfirmCheckupAppointmentRequest | ConfirmLfkAppointmentRequest;
