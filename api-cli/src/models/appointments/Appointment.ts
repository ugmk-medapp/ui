import { IPatient } from '../patients';
import { ServiceType } from './ServiceType';

export interface Appointment {
    name: string;
    appTypeId: string;
    appointmentId: number;
    patient: IPatient;
    quantity: number;
    serviceType: ServiceType;
    comment: string;
}
