export interface IAppointmentType {
    appTypeId: number;
    name: string;
}