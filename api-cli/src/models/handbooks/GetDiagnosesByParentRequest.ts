export interface GetDiagnosesByParentRequest {
    medDepId: string;
    parentId: string;
    page: number;
}
