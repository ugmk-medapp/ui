export interface GetDiagnosesBySearchStringRequest {
    searchString: string;
    medDepId: string;
    page: number;
}
