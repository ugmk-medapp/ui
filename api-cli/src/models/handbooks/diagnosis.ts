export interface Diagnosis {
    diagnosisId: number;
    parentId: string;
    title: string;
    code: string;
    forbidden: boolean;
}
