import { DiagnosticResult } from '.';

export interface CheckupDiagnosticResult extends DiagnosticResult {
    complaints?: string;
    diagnosisId?: number;
    diagnosis?: string;
    diagnosisDescription?: string;
    outcome?: string;
    outcomeId?: number;
    result?: string;
    resultId?: number;
    diseaseHistory?: string;
    objectively?: string;
    recommendations?: string;
    isClose?: boolean;
    expertAnamnesis?: string;
}
