import { DiagnosticResult } from '.';

export interface LfkDiagnosticResult extends DiagnosticResult {
    complaints?: string;
    program?: string;
    progression?: string;
    APbefore?: string;
    APafter?: string;
}
