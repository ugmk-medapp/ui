import { CheckupDiagnosticResult, LfkDiagnosticResult } from '.';

export type DiagnosticResultType = LfkDiagnosticResult | CheckupDiagnosticResult;
