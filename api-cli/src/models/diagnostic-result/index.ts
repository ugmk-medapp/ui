export * from './diagnostic-result';
export * from './lfk-diagnostic-result';
export * from './checkup-diagnostic-result';
export * from './diagnostic-result-type';
