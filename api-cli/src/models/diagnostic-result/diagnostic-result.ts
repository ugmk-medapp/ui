import { ServiceType } from '../appointments';

export interface DiagnosticResult {
    id: number;
    doctorFullName: string;
    dateConsultation: string;
    serviceType: ServiceType;
}
