export * from './IPatient';
export * from './GetPatientsRequest';
export * from './GetPatientCardRequest';
export * from './IPatientExtended';
