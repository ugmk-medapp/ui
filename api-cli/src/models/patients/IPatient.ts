export interface IPatient {
    name: string;
    surname: string;
    patronymic: string;
    birthYear: string | null;
    patientId: string;
    appointmentTime: string | null;
}
