export interface GetPatientsRequest {
    page: number;
    medDepId: string;
    searchName?: string;
}