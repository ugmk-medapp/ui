import { IPatient } from '.';
import { DiagnosticResultType } from '../diagnostic-result';

export interface IPatientExtended extends IPatient {
    diagnosticResults: DiagnosticResultType[];
}
