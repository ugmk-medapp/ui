export interface GetPatientCardRequest {
    page: number;
    medDepId: string;
    searchString?: string;
}