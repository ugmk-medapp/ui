import { ServiceType } from '..';

export interface Template {
    _id: string;
    user: string;
    serviceType: ServiceType;
    title: string;
}
