import { Template } from '.';

export interface LfkTemplate extends Template {
    complaints?: string;
    program?: string;
    progression?: string;
    APbefore?: string;
    APafter?: string;
}
