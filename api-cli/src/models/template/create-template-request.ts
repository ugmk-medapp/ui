// Это жесткий скам, а не проект)) Вся его суть выражается в этом интерфейсе.
// Но во всем надо искать плюсы! Работая над этим проектом, я набил много шишек,

import { ServiceType } from '..';

// и понял (а точнее убедился), что НЕЛЬЗЯ и что НЕ НАДО делать в процессах разработки IT проекта
export interface CreateTemplateRequest {
    medDepId: string;
    serviceType: ServiceType;
    title: string;
    complaints?: string; // жалобы (они общие для всех)

    //ЛФК
    program?: string;
    progression?: string;
    APbefore?: string;
    APafter?: string;

    //Осмотр
    diagnosisId?: number;
    diagnosisDescription?: string;
    outcomeId?: number;
    outcome?: string;
    resultId?: number;
    result?: string;
    diseaseHistory?: string;
    objectively?: string;
    recommendations?: string;
    isClose?: boolean;
    expertAnamnesis?: string;
}
