import { LfkTemplate, CheckupTemplate } from '.';

export type TemplateType = LfkTemplate | CheckupTemplate;
