import { Template } from '.';
import { Diagnosis, Outcome, Result } from '..';

export interface CheckupTemplate extends Template {
    complaints?: string;
    diagnosis?: Diagnosis;
    diagnosisDescription?: string;
    outcome?: Outcome;
    result?: Result;
    diseaseHistory?: string;
    objectively?: string;
    recommendations?: string;
    isClose?: boolean;
    expertAnamnesis?: string;
}
