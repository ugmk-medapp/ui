export interface ISendCodeRequest {
    email: string;
    action: "C" | "U";
}