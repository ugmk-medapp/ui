export interface IStaffUnit {
    medDepId: string;
    externalMedDepId: string;
    name: string;
}
