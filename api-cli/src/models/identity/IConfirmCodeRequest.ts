export interface IConfirmCodeRequest {
    code: string;
    email: string;
    action: "U" | "C";
}