import { IStaffUnit } from './IStaffUnit';

export interface IUser {
    _id: string;
    external_id: string;
    is_verified: boolean;
    surnameAndInitials: string;
    email: string;
    staffUnits: IStaffUnit[];
}
