export * from './identity';
export * from './appointments';
export * from './patients';
export * from './handbooks';
export * from './diagnostic-result';
export * from './template';
