import { GetPatientCardRequest, IPatient, IPatientExtended, GetPatientsRequest } from "../models";
import Api from "../utils/api";

export class PatientApi {
    static getPatients(request: GetPatientsRequest) {
        return Api.get<IPatient[]>('/patients', { params: {...request} });
    }

    static getPatientCard(patientId: string, request: GetPatientCardRequest) {
        return Api.get<IPatientExtended>(`/patients/${patientId}/card`, { params: {...request} });
    }
}