import { Appointment, ServiceType } from '..';

export const APPOINTMENT_CHECKUP: Appointment = {
    name: 'Наименование',
    appTypeId: '',
    appointmentId: 1,
    patient: {
        name: 'Иван',
        surname: 'Иванов',
        patronymic: 'Иванович',
        birthYear: '20.08.1996',
        patientId: 'ASD2',
        appointmentTime: '12:30',
    },
    quantity: 1,
    serviceType: ServiceType.Checkup,
    comment: 'Комментарий',
};

export const APPOINTMENT_LFK: Appointment = {
    name: 'Наименование',
    appTypeId: '',
    appointmentId: 1,
    patient: {
        name: 'Иван',
        surname: 'Иванов',
        patronymic: 'Иванович',
        birthYear: '20.08.1996',
        patientId: 'ASD2',
        appointmentTime: '12:30',
    },
    quantity: 1,
    serviceType: ServiceType.LFK,
    comment: 'Комментарий',
};
