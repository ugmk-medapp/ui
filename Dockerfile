FROM node:15.11.0-alpine3.10 as build-step

WORKDIR /app

COPY api-cli api-cli

RUN cd api-cli && yarn && yarn run build

RUN cd api-cli && yarn link 

COPY web/package*.json web/

RUN cd web && yarn --production

RUN cd web && yarn add file:../api-cli

RUN cd web && yarn link @ugmk/api

COPY web web

EXPOSE 4000

RUN cd web && yarn run build

# Stage 2
FROM nginx:1.19.7-alpine

RUN rm -rf /usr/share/nginx/html/*

COPY /web/.nginx/nginx.conf /etc/nginx/nginx.conf

COPY --from=build-step /app/web/build /usr/share/nginx/html

COPY /web/env.sh /usr/share/nginx/html

WORKDIR /usr/share/nginx/html

RUN apk add --no-cache bash
RUN chmod +x env.sh

CMD ["/bin/bash", "-c", "/usr/share/nginx/html/env.sh && nginx -g \"daemon off;\""]
