import moment from "moment";

export function convertDateToDDMMYYYY(dateString: string) {
    return moment(new Date(dateString))
    .format('DD/MM/YYYY')
    .toString()
    .split('/')
    .join('.');
}