import { AxiosError, DiagnosticResultType } from '@ugmk/api';
import {
    CloseSaveAsTemplateModal,
    OpenSaveAsTemplateModal,
    PatientCardPageActions,
    SaveAsTemplate,
    SaveAsTemplateError,
    SaveAsTemplateSuccess,
} from './patient-card.actions';

export interface PatientCardPageState {
    isLoadingSaveAsTemplate: boolean;
    errorSaveAsTemplate?: AxiosError;
    isSaveAsTemplateModalOpened: boolean;
    saveAsTemplateData?: DiagnosticResultType;
}

const initialState: PatientCardPageState = {
    isLoadingSaveAsTemplate: false,
    errorSaveAsTemplate: undefined,
    isSaveAsTemplateModalOpened: false,
    saveAsTemplateData: undefined,
};

export function patientCardPageReducer(state = initialState, action: PatientCardPageActions): PatientCardPageState {
    switch (action.type) {
        case SaveAsTemplate.Name: {
            return {
                ...state,
                isLoadingSaveAsTemplate: true,
            };
        }

        case SaveAsTemplateSuccess.Name: {
            return {
                ...state,
                isLoadingSaveAsTemplate: false,
            };
        }

        case SaveAsTemplateError.Name: {
            return {
                ...state,
                isLoadingSaveAsTemplate: false,
                errorSaveAsTemplate: action.error,
            };
        }

        case OpenSaveAsTemplateModal.Name: {
            return {
                ...state,
                isSaveAsTemplateModalOpened: true,
                saveAsTemplateData: action.saveRequest,
            };
        }

        case CloseSaveAsTemplateModal.Name: {
            return {
                ...state,
                isSaveAsTemplateModalOpened: false,
                saveAsTemplateData: undefined,
            };
        }
        default: {
            return state;
        }
    }
}
