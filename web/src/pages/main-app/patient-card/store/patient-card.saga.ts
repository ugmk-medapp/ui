import { select, takeLatest } from '@redux-saga/core/effects';
import { SagaIterator } from '@redux-saga/types';
import {
    ApiResponse,
    CheckupDiagnosticResult,
    CheckupTemplate,
    LfkDiagnosticResult,
    LfkTemplate,
    Outcome,
    Result,
    ServiceType,
    TemplateApi,
    TemplateType,
} from '@ugmk/api';
import { goBack } from 'connected-react-router';
import { SelectTemplate } from 'pages/main-app/appointment/store/appointment.actions';
import { call, put } from 'redux-saga/effects';
import { RootState } from 'store/root.reducer';
import {
    AddEmptyTemplateToSelectedTemplate,
    CloseSaveAsTemplateModal,
    SaveAsTemplate,
    SaveAsTemplateError,
    SaveAsTemplateSuccess,
} from './patient-card.actions';

function* saveAsTemplateHandler(action: SaveAsTemplate): SagaIterator {
    const { data, error }: ApiResponse<TemplateType> = yield call(TemplateApi.createTemplate, action.request);

    if (data) {
        yield put({ ...new SaveAsTemplateSuccess() });
        yield put({ ...new CloseSaveAsTemplateModal() });
    } else if (error) {
        yield put({ ...new SaveAsTemplateError(error) });
    }
}

function* addEmptyTemplateToSelectedTemplateHandler(action: AddEmptyTemplateToSelectedTemplate): SagaIterator {
    const outcomes: Outcome[] = yield select((rootState: RootState) => rootState.handbooks.outcomes);
    const results: Result[] = yield select((rootState: RootState) => rootState.handbooks.results);

    if (action.diagnosticResult.serviceType === ServiceType.Checkup) {
        const diagnosticResult = action.diagnosticResult as CheckupDiagnosticResult;
        const emptyTemplate: CheckupTemplate = {
            _id: '0',
            user: '',
            title: 'empty',
            ...diagnosticResult,
            diagnosis: undefined,
            outcome: outcomes.find((outcome) => outcome.outcomeId === diagnosticResult.outcomeId),
            result: results.find((result) => result.resultId === diagnosticResult.resultId),
        };

        yield put({ ...new SelectTemplate(emptyTemplate) });
        yield put(goBack());
    }

    if (action.diagnosticResult.serviceType === ServiceType.LFK) {
        const diagnosticResult = action.diagnosticResult as LfkDiagnosticResult;
        const emptyTemplate: LfkTemplate = {
            _id: '0',
            user: '',
            title: 'empty',
            ...diagnosticResult,
        };

        yield put({ ...new SelectTemplate(emptyTemplate) });
        yield put(goBack());
    }
}

export default function* patientCardPageSaga(): SagaIterator {
    yield takeLatest(SaveAsTemplate.Name, saveAsTemplateHandler);
    yield takeLatest(AddEmptyTemplateToSelectedTemplate.Name, addEmptyTemplateToSelectedTemplateHandler);
}
