import { Expander } from 'components/base/expander';
import { IconType } from 'components/base/icon/types';
import Loader from 'components/base/Loader';
import Typography from 'components/base/Typography';
import MainLayout from 'components/layouts/MainLayout';
import { MainLayoutContext } from 'components/layouts/MainLayout/state/MainLayoutProvider';
import PatientCardResultsSearchForm from 'components/partial/patient-card-results-search-form';
import PatientResultCardFeed from 'pages/main-app/patient-card/components/patient-result-card-feed';
import { convertDateToDDMMYYYY } from 'helpers';
import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useRouteMatch } from 'react-router';
import { Routes } from 'router/routes';
import { ClearPageResultsPatientCardAction, SetCardResultsFiltersAction } from 'store/patients/patients.actions';
import { RootState } from 'store/root.reducer';
import SaveAsTemplateModal from './components/save-as-template-modal/save-as-template-modal.component';
import * as S from './styles';
import Icon from 'components/base/icon';
import { ServiceType } from '@ugmk/api';
import { CheckupFormData } from '../appointment/components/appointment-form/checkup-form/checkup-form.component';
import { LFKFormData } from '../appointment/components/appointment-form/lfk-form/lfk-form.component';

export interface PatientCardPageRouteParams {
    fromAppointment: boolean;
    serviceType: ServiceType;
    oldAppointmentState: CheckupFormData | LFKFormData | null;
}

const PatientCardPage: React.FC = () => {
    const history = useHistory<PatientCardPageRouteParams>();
    const dispatch = useDispatch();

    const routeMatch = useRouteMatch<{ id?: string }>(Routes.PatientCard);
    const patientId = routeMatch?.params.id;

    const currentMedDepId = useSelector((state: RootState) => state.auth.currentStaffUnit?.medDepId);
    const isLoadingCard = useSelector((state: RootState) => state.patient.isLoadingCard);
    const patientCard = useSelector((state: RootState) => state.patient.patientCard);

    const fromAppointmentState = useMemo(() => history.location.state, [history]);
    const backButtonOptions = useMemo(() => {
        if (fromAppointmentState?.fromAppointment) {
            return {
                title: 'Назад к карточке',
                onClick: () => history.goBack(),
            };
        } else {
            return {
                title: 'Вернуться к поиску пациентов',
                onClick: () => history.push(Routes.PatientCardList),
            };
        }
    }, [fromAppointmentState, history]);

    useEffect(() => {
        if (patientId && currentMedDepId) {
            dispatch({
                ...new SetCardResultsFiltersAction(
                    {
                        patientId,
                    },
                    currentMedDepId,
                ),
            });
        }
    }, [dispatch, patientId, currentMedDepId]);

    useEffect(() => {
        return () => {
            dispatch({ ...new ClearPageResultsPatientCardAction() });
        };
    }, [dispatch]);

    if (isLoadingCard) {
        return <Loader />;
    }

    return (
        <MainLayout>
            <MainLayout.Header>
                <S.PatientCardHeader>
                    <S.BackButton onClick={backButtonOptions.onClick}>
                        <S.BackButtonIcon as={Icon} type={IconType.Arrow} />
                        {backButtonOptions.title}
                    </S.BackButton>
                    {patientCard && (
                        <React.Fragment>
                            <S.PatientCardHeaderRow>
                                <Typography type="h3" size={17} weight="medium" marginBottom={0} textAlign="left">
                                    {patientCard.surname} {patientCard.name} {patientCard.patronymic[0]}.
                                </Typography>
                                <Typography type="span" size={14} weight="regular" marginBottom={0}>
                                    ({patientCard.birthYear} г.)
                                </Typography>
                            </S.PatientCardHeaderRow>
                            {patientCard.diagnosticResults[0] && (
                                <Typography type="span" size={14} marginBottom={20} textAlign="left">
                                    Последнее посещение:{' '}
                                    {convertDateToDDMMYYYY(patientCard.diagnosticResults[0].dateConsultation)}
                                </Typography>
                            )}

                            <MainLayoutContext.Consumer>
                                {({ headerActive, setHeaderActive }) => (
                                    <Expander isActive={headerActive}>
                                        {currentMedDepId && (
                                            <PatientCardResultsSearchForm
                                                currentMedDepId={currentMedDepId}
                                                onAfterSearch={() => setHeaderActive(false)}
                                            />
                                        )}
                                    </Expander>
                                )}
                            </MainLayoutContext.Consumer>
                        </React.Fragment>
                    )}
                </S.PatientCardHeader>
            </MainLayout.Header>
            <PatientResultCardFeed
                patientId={routeMatch?.params.id}
                medDepId={currentMedDepId}
                fromAppointmentState={fromAppointmentState}
            />
            <MainLayout.MenuNavigation />
            <SaveAsTemplateModal />
        </MainLayout>
    );
};

export default PatientCardPage;
