import { DiagnosisSearchScreen } from 'components/partial/diagnosis-search-screen';
import { AppointmentPageType } from 'models/appointment/appointment-page-type';
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Routes } from 'router/routes';
import AppointmentPage from './appointment/appointment.component';
import PatientCardPage from './patient-card';
import { PatientCardListPage } from './patient-card-list';
import { SchedulePage } from './schedule';
import { ServicePage } from './service';

const MainApp: React.FC = () => {
    return (
        <>
            <Switch>
                <Route exact path={Routes.Service} component={ServicePage} />
                <Route exact path={Routes.Schedule} component={SchedulePage} />
                <Route exact path={Routes.PatientCardList} component={PatientCardListPage} />
                <Route exact path={Routes.PatientCard} component={PatientCardPage} />
                <Route
                    exact
                    path={Routes.AppointmentSchedule}
                    component={() => <AppointmentPage pageType={AppointmentPageType.Schedule} />}
                />
                <Route
                    exact
                    path={Routes.AppointmentService}
                    component={() => <AppointmentPage pageType={AppointmentPageType.Service} />}
                />
            </Switch>
            <Redirect to={Routes.Schedule} />
            <DiagnosisSearchScreen />
        </>
    );
};

export default MainApp;
