import { Expander } from 'components/base/expander';
import { useMainLayoutState } from 'components/layouts/MainLayout/state/MainLayoutProvider';
import { AppointmentSearchForm } from 'components/partial/appointment-search-form';
import StuffUnitDropdown from 'components/partial/stuff-unit-dropdown';
import React from 'react';

const ServicePageHeader: React.FC = () => {
    const { headerActive, setHeaderActive } = useMainLayoutState();
    return (
        <>
            <StuffUnitDropdown />
            <Expander isActive={headerActive}>
                <AppointmentSearchForm onAfterSearch={() => setHeaderActive(false)} />
            </Expander>
        </>
    );
};

export default ServicePageHeader;
