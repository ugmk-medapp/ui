import MainLayout from 'components/layouts/MainLayout';
import PreContentLayout from 'components/layouts/PreContentLayout';
import React, { useEffect, useState, useRef, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { generatePath, useHistory, useParams } from 'react-router-dom';
import { Routes } from 'router/routes';
import AppointmentHeader from './components/appointment-header/appointment-header.component';
import {
    ClearAppointmentPageStore,
    GetAppointmentById,
    GetTemplates,
    OpenTemplateSelectionScreen,
} from './store/appointment.actions';
import { appointmentPageSelector, appointmentPatientIdSelector } from './store/appointment.selectors';
import * as S from './appointment.styles';
import { Button } from 'components/base/button';
import AppointmentForm from './components/appointment-form/appointment-form.component';
import { IconType } from 'components/base/icon/types';
import { currentStaffUnitSelector } from 'store/auth/auth.selectors';
import { GetOutcomes, GetResults } from 'store/handbooks/handbooks.actions';
import { AppointmentPageType } from 'models/appointment/appointment-page-type';
import TemplateSelectionScreen from './components/template-selection-screen/template-selection-screen.component';
import { PatientCardPageRouteParams } from '../patient-card';
import { CheckupFormData } from './components/appointment-form/checkup-form/checkup-form.component';
import { LFKFormData } from './components/appointment-form/lfk-form/lfk-form.component';
import { LocalStorageService } from 'helpers/local-storage-service/local-storage-service';
import { LocalStorageKey } from 'helpers/local-storage-service/local-storage-service.types';

interface AppointmentPageRouteParams {
    id: string;
}

interface Props {
    pageType: AppointmentPageType;
}

const AppointmentPage: React.FC<Props> = ({ pageType }) => {
    const history = useHistory();
    const [savedFormData, setSavedFormData] = useState<CheckupFormData | LFKFormData | null>(null);
    const currentFormStateRef = useRef<CheckupFormData | LFKFormData | null>(null);
    const dispatch = useDispatch();
    const currentStaffUnit = useSelector(currentStaffUnitSelector);
    const pageParams = useParams<AppointmentPageRouteParams>();
    const patientId = useSelector(appointmentPatientIdSelector);
    const { isLoadingAppointment, appointment } = useSelector(appointmentPageSelector);

    useEffect(() => {
        const { id } = pageParams;

        if (id && !Number.isNaN(+id) && currentStaffUnit) {
            dispatch({ ...new GetAppointmentById(+id, currentStaffUnit.medDepId, pageType) });
        } else {
            history.replace(Routes.Schedule);
        }
    }, [history, dispatch, pageParams, pageType, currentStaffUnit]);

    useEffect(() => {
        if (currentStaffUnit) {
            dispatch({ ...new GetResults(currentStaffUnit.medDepId) });
            dispatch({ ...new GetOutcomes(currentStaffUnit.medDepId) });
        }
    }, [dispatch, currentStaffUnit]);

    useEffect(() => {
        if (!appointment) return;

        dispatch({ ...new GetTemplates(appointment.serviceType) });
    }, [appointment, dispatch]);

    useEffect(() => {
        if (history.action === 'POP') {
            setSavedFormData(LocalStorageService.getItem(LocalStorageKey.SAVED_APPOINTMENT_FORM_DATA));
            LocalStorageService.removeItem(LocalStorageKey.SAVED_APPOINTMENT_FORM_DATA);
        }
    }, [history]);

    useEffect(() => {
        return () => {
            dispatch({ ...new ClearAppointmentPageStore() });
        };
    }, [dispatch]);

    const onFormChange = useCallback(
        (formState: CheckupFormData | LFKFormData) => {
            currentFormStateRef.current = formState;
        },
        [currentFormStateRef],
    );

    const openTemplateSelectionScreen = () => {
        if (appointment) {
            dispatch({ ...new OpenTemplateSelectionScreen(appointment.serviceType) });
        }
    };

    return (
        <MainLayout>
            <PreContentLayout isLoading={isLoadingAppointment}>
                <MainLayout.Header isShowSearchButton={false}>
                    <AppointmentHeader />
                </MainLayout.Header>
                <S.Container>
                    <S.Content>
                        <Button
                            label="Выбрать шаблон"
                            size="medium"
                            theme="ghost"
                            onClick={openTemplateSelectionScreen}
                        />
                        <AppointmentForm
                            savedFormData={savedFormData}
                            onFormChange={onFormChange}
                            onClear={() => setSavedFormData(null)}
                        />
                        {patientId && (
                            <Button
                                label="Карта пациента"
                                size="medium"
                                theme="ghost"
                                type="button"
                                side="left"
                                icon={IconType.PatientCard}
                                onClick={() => {
                                    LocalStorageService.setItem(
                                        LocalStorageKey.SAVED_APPOINTMENT_FORM_DATA,
                                        currentFormStateRef.current,
                                    );
                                    history.push(generatePath(Routes.PatientCard, { id: patientId }), {
                                        fromAppointment: true,
                                        serviceType: appointment?.serviceType,
                                        oldAppointmentState: currentFormStateRef.current,
                                    } as PatientCardPageRouteParams);
                                }}
                            />
                        )}
                    </S.Content>
                </S.Container>
            </PreContentLayout>
            <MainLayout.MenuNavigation />
            <TemplateSelectionScreen />
        </MainLayout>
    );
};

export default AppointmentPage;
