import { CheckupTemplate, LfkTemplate, Template } from '@ugmk/api';

export type CheckupTemplateInfoKey = keyof Omit<CheckupTemplate, keyof Template>;
export type CheckupTemplateLabelsMapType = {
    [key in CheckupTemplateInfoKey]: string;
};

export type LfkTemplateInfoKey = keyof Omit<LfkTemplate, keyof Template>;
export type LfkTemplateLabelsMapType = {
    [key in LfkTemplateInfoKey]: string;
};
