import React, { useEffect, useMemo } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useSelector } from 'react-redux';
import { RootState } from 'store/root.reducer';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers';
import * as S from './checkup-form.styles';
import { Textarea } from 'components/base/input';
import Switcher from 'components/base/Switcher';
import { Dropdown } from 'components/base/dropdown';
import { Button } from 'components/base/button';
import { DiagnosisField } from './components/diagnosis-field';
import { CheckupTemplate } from '@ugmk/api';
import { LocalStorageService } from 'helpers/local-storage-service/local-storage-service';
import { LocalStorageKey } from 'helpers/local-storage-service/local-storage-service.types';

export type CheckupFormData = {
    diagnosisId?: number | null;
    diagnosisDescription?: string | null;
    isClose?: boolean | null;
    outcomeId?: number | null;
    resultId?: number | null;
    complaints?: string | null;
    expertAnamnesis?: string | null;
    diseaseHistory?: string | null;
    objectively?: string | null;
    recommendations?: string | null;
    sendToLK?: boolean | null;
};

const schema = yup.object().shape({
    diagnosisId: yup.number().nullable().notRequired(),
    diagnosisDescription: yup.string().nullable().notRequired(),
    outcomeId: yup
        .string()
        .nullable()
        .when('isClose', {
            is: true,
            then: yup.string().required('Выберите исход'),
        }),
    resultId: yup
        .string()
        .nullable()
        .when('isClose', {
            is: true,
            then: yup.string().required('Выберите результат'),
        }),
    complaints: yup.string().nullable().notRequired(),
    expertAnamnesis: yup.string().nullable().notRequired(),
    diseaseHistory: yup.string().nullable().notRequired(),
    objectively: yup.string().nullable().notRequired(),
    recommendations: yup.string().nullable().notRequired(),
    isClose: yup.boolean().nullable(),
    sendToLK: yup.boolean().nullable(),
});

interface Props {
    savedFormData?: CheckupFormData | null;
    selectedTemplate?: CheckupTemplate;
    onFormChange?: (data: CheckupFormData) => void;
    onConfirm: (data: CheckupFormData) => void;
    onClear: () => void;
}

const CheckupForm: React.FC<Props> = ({ onConfirm, onFormChange, onClear, selectedTemplate, savedFormData }) => {
    const results = useSelector((root: RootState) => root.handbooks.results);
    const outcomes = useSelector((root: RootState) => root.handbooks.outcomes);
    const isConfirmPending = useSelector((root: RootState) => root.appointments.isConfirmPending);

    const {
        handleSubmit,
        control,
        watch,
        setValue,
        getValues,
        formState: { errors },
    } = useForm<CheckupFormData>({
        resolver: yupResolver(schema),
    });

    useEffect(() => {
        if (onFormChange) {
            onFormChange(watch());
        }
    }, [watch, onFormChange]);

    useEffect(() => {
        if (!selectedTemplate) return;
        if (selectedTemplate.diagnosis) {
            LocalStorageService.setItem(LocalStorageKey.SAVED_DIAGNOSIS_FORM_DATA, selectedTemplate.diagnosis);
        }
        setValue('diagnosisId', selectedTemplate.diagnosis?.diagnosisId);
        setValue('diagnosisDescription', selectedTemplate.diagnosisDescription);
        setValue('outcomeId', selectedTemplate.outcome?.outcomeId);
        setValue('resultId', selectedTemplate.result?.resultId);
        setValue('complaints', selectedTemplate.complaints);
        setValue('expertAnamnesis', selectedTemplate.expertAnamnesis);
        setValue('diseaseHistory', selectedTemplate.diseaseHistory);
        setValue('objectively', selectedTemplate.objectively);
        setValue('recommendations', selectedTemplate.recommendations);
        setValue('isClose', selectedTemplate.isClose);
    }, [selectedTemplate, setValue]);

    const savedFormDiagnosis = useMemo(() => {
        if (savedFormData && savedFormData.diagnosisId) {
            const savedDiagnosis = LocalStorageService.getItem(LocalStorageKey.SAVED_DIAGNOSIS_FORM_DATA);
            if (savedDiagnosis?.diagnosisId === savedFormData.diagnosisId) {
                return savedDiagnosis;
            }
            return undefined;
        }
    }, [savedFormData]);

    const onConfirmHandler = handleSubmit(
        (data: CheckupFormData) => {
            onConfirm(data);
            onClear();
        },
        (errors) => {
            console.log('Confirm error: ', errors, getValues());
        },
    );
    const isCloseValue = watch().isClose;

    return (
        <S.Form>
            <Controller
                name="complaints"
                control={control}
                defaultValue={selectedTemplate?.complaints || savedFormData?.complaints || ''}
                render={(props) => {
                    return <Textarea disabled={isConfirmPending} value={props.value || ''} onChange={props.onChange} label="Жалобы" />;
                }}
            />

            <Controller
                name="expertAnamnesis"
                control={control}
                defaultValue={selectedTemplate?.expertAnamnesis || savedFormData?.expertAnamnesis || ''}
                render={(props) => {
                    return <Textarea disabled={isConfirmPending} value={props.value || ''} onChange={props.onChange} label="Экспертный анамнез" />;
                }}
            />

            <Controller
                name="diseaseHistory"
                control={control}
                defaultValue={selectedTemplate?.diseaseHistory || savedFormData?.diseaseHistory || ''}
                render={(props) => {
                    return <Textarea disabled={isConfirmPending} value={props.value || ''} onChange={props.onChange} label="История заболевания" />;
                }}
            />

            <Controller
                name="objectively"
                control={control}
                defaultValue={selectedTemplate?.objectively || savedFormData?.objectively || ''}
                render={(props) => {
                    return <Textarea disabled={isConfirmPending} value={props.value || ''} onChange={props.onChange} label="Объективно" />;
                }}
            />

            <Controller
                name="diagnosisId"
                defaultValue={selectedTemplate?.diagnosis?.diagnosisId || savedFormDiagnosis?.diagnosisId || null}
                control={control}
                render={(props) => {
                    return (
                        <DiagnosisField
                            value={selectedTemplate?.diagnosis || savedFormDiagnosis}
                            onSelectCode={(diagnosis) => {
                                LocalStorageService.setItem(LocalStorageKey.SAVED_DIAGNOSIS_FORM_DATA, diagnosis);
                                props.onChange(diagnosis.diagnosisId);
                                setValue(
                                    'diagnosisDescription',
                                    diagnosis.title + (getValues().diagnosisDescription || ''),
                                );
                            }}
                        />
                    );
                }}
            />

            <Controller
                name="diagnosisDescription"
                defaultValue={selectedTemplate?.diagnosisDescription || savedFormData?.diagnosisDescription || ''}
                control={control}
                render={(props) => {
                    return <Textarea disabled={isConfirmPending} value={props.value || ''} onChange={props.onChange} label="Диагноз" />;
                }}
            />

            <Controller
                name="recommendations"
                control={control}
                defaultValue={selectedTemplate?.recommendations || savedFormData?.recommendations || ''}
                render={(props) => {
                    return <Textarea disabled={isConfirmPending} value={props.value || ''} onChange={props.onChange} label="Рекомендации" />;
                }}
            />

            <Controller
                name="isClose"
                control={control}
                render={(props) => {
                    return (
                        <Switcher
                            defaultValue={
                                selectedTemplate?.isClose !== undefined
                                    ? selectedTemplate?.isClose
                                    : savedFormData?.isClose !== undefined
                                    ? savedFormData?.isClose
                                    : props.value
                            }
                            label="Случай закрыт"
                            onChange={props.onChange}
                        />
                    );
                }}
                defaultValue={
                    selectedTemplate?.isClose !== undefined
                        ? selectedTemplate?.isClose
                        : savedFormData?.isClose !== undefined
                        ? savedFormData?.isClose
                        : true
                }
            />
            {isCloseValue && (
                <>
                    <Controller
                        name="resultId"
                        control={control}
                        render={(props) => {
                            return (
                                <Dropdown
                                    isDisabled={isConfirmPending}
                                    error={errors.resultId?.message}
                                    label="Результат"
                                    items={results}
                                    value={
                                        selectedTemplate?.result ||
                                        results.find((r) => r.resultId === savedFormData?.resultId)
                                    }
                                    onSelect={(result) => {
                                        props.onChange(result.resultId);
                                    }}
                                    labelSelector={(item) => item.title}
                                />
                            );
                        }}
                        defaultValue={selectedTemplate?.result?.resultId || savedFormData?.resultId || null}
                    />
                    <Controller
                        name="outcomeId"
                        control={control}
                        render={(props) => {
                            return (
                                <Dropdown
                                    isDisabled={isConfirmPending}
                                    error={errors.outcomeId?.message}
                                    label="Исход"
                                    items={outcomes}
                                    value={
                                        selectedTemplate?.outcome ||
                                        outcomes.find((o) => o.outcomeId === savedFormData?.outcomeId)
                                    }
                                    onSelect={(outcome) => {
                                        props.onChange(outcome.outcomeId);
                                    }}
                                    labelSelector={(item) => item.title}
                                />
                            );
                        }}
                        defaultValue={selectedTemplate?.outcome?.outcomeId || savedFormData?.outcomeId || null}
                    />
                </>
            )}
            <Controller
                name="sendToLK"
                control={control}
                render={(props) => {
                    return (
                        <Switcher
                            defaultValue={savedFormData?.sendToLK || props.value}
                            label="Выгрузить в личный кабинет"
                            onChange={props.onChange}
                        />
                    );
                }}
                defaultValue={false}
            />
            {isConfirmPending ? (
                <Button disabled load={true} />
            ) : (
                <Button label="Подтвердить услугу" size="medium" onClick={onConfirmHandler} />
            )}
        </S.Form>
    );
};

export default CheckupForm;
