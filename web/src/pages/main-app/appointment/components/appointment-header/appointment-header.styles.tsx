import styled from 'styled-components';
import { Colors } from 'styles/colors';

export const BackButtonIcon = styled.span`
    transform: rotate(180deg);
`;

export const BackButton = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    color: ${Colors.Orange};
    font-weight: 500;

    ${BackButtonIcon} {
        margin-right: 12px;
    }
`;

export const Container = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;

    ${BackButton} {
        margin-bottom: 15px;
    }
`;
