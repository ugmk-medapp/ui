import Icon from 'components/base/icon';
import { IconType } from 'components/base/icon/types';
import { Tooltip } from 'components/base/tooltip/tooltip.component';
import Typography from 'components/base/Typography';
import { getPatientCardName } from 'helpers/patient';
import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { appointmentPageSelector } from '../../store/appointment.selectors';
import * as S from './appointment-header.styles';

const AppointmentHeader: React.FC = () => {
    const history = useHistory();
    const { appointment } = useSelector(appointmentPageSelector);
    const fullDescription = `${appointment?.name} (${appointment?.quantity})`;
    const shortDescription = fullDescription.split(' ').length > 7 ? fullDescription.split(' ').slice(0, 7).join(' ') + '...' : fullDescription;

    return (
        <S.Container>
            <S.BackButton onClick={history.goBack}>
                <S.BackButtonIcon as={Icon} type={IconType.Arrow} />
                Вернуться назад
            </S.BackButton>
            {appointment && (
                <React.Fragment>
                    <Typography type="span" weight="medium" size={17} marginBottom={10} textAlign="left">
                        {getPatientCardName(appointment.patient)}{' '}
                        <Typography type="span">({appointment.patient.birthYear} г.)</Typography>
                    </Typography>
                    <Tooltip label={fullDescription}>
                        <Typography weight="medium" marginBottom={0} size={14} type="p" textAlign="left">
                            {shortDescription}
                        </Typography>
                    </Tooltip>
                </React.Fragment>
            )}
        </S.Container>
    );
};

export default AppointmentHeader;
