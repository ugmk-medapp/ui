import MainLayout from 'components/layouts/MainLayout';
import PatientCard from 'components/partial/patient-card';
import usePaginateByScroll from 'hooks/usePaginateByScroll';
import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { currentStaffUnitSelector } from 'store/auth/auth.selectors';
import { GetPatientsAction, LoadNextPagePatientsAction } from 'store/patients/patients.actions';
import { RootState } from 'store/root.reducer';
import PatientCardListPageHeader from './components/patient-card-list-page-header/patient-card-list-page-header';
import * as S from './patient-card-list.styles';

export const PatientCardListPage: React.FC = () => {
    const dispatch = useDispatch();
    const feedContainer = useRef<HTMLDivElement | null>(null);
    const currentStaffUnit = useSelector(currentStaffUnitSelector);
    const patientCards = useSelector((state: RootState) => state.patient.patients);

    usePaginateByScroll(feedContainer, () => {
        if (currentStaffUnit) {
            dispatch({ ...new LoadNextPagePatientsAction(currentStaffUnit.medDepId) });
        }
    });

    useEffect(() => {
        if (currentStaffUnit) {
            dispatch({ ...new GetPatientsAction(currentStaffUnit.medDepId) });
        }
    }, [dispatch, currentStaffUnit]);

    return (
        <MainLayout>
            <MainLayout.Header>
                <PatientCardListPageHeader />
            </MainLayout.Header>
            <S.Container ref={feedContainer}>
                {patientCards.map((patientCard) => (
                    <PatientCard
                        name={patientCard.name}
                        surname={patientCard.surname}
                        patronymic={patientCard.patronymic}
                        birthYear={patientCard.birthYear}
                        patientId={patientCard.patientId}
                        key={patientCard.patientId}
                    />
                ))}
            </S.Container>
            <MainLayout.HelpLink />
            <MainLayout.MenuNavigation />
        </MainLayout>
    );
};
