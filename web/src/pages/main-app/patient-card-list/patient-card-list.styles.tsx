import { PatientCardContainer } from 'components/partial/patient-card/styles';
import styled from 'styled-components';

export const Container = styled.div`
    padding: 20px 25px 60px;
    flex: 1;
    overflow: auto;

    ${PatientCardContainer} {
        margin-top: 20px;

        &:last-of-type {
            margin-bottom: 20px;
        }
    }
`;
