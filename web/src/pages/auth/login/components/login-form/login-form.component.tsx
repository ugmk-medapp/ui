import React from 'react';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers';
import { useForm } from 'react-hook-form';
import { Button } from 'components/base/button';
import { useDispatch } from 'react-redux';
import { LogInAction } from 'store/auth/auth.actions';
import { InputText } from 'components/base/input';
import * as S from './login-form.styles';

type FormData = {
    email: string;
    password: string;
};

const schema = yup.object().shape({
    email: yup.string().required('Поле почты является обязательным').email('Введите почту корректно'),
    password: yup.string().required('Поле пароля является обязательным'),
});

export const LoginForm: React.FC = () => {
    const dispatch = useDispatch();
    const { register, handleSubmit, errors, reset } = useForm<FormData>({
        resolver: yupResolver(schema),
    });

    const onSubmit = handleSubmit((data) => {
        dispatch({ ...new LogInAction(data.email, data.password) });
        reset();
    });
    return (
        <S.Form onSubmit={onSubmit}>
            <InputText isClear={true} name="email" label="Ваша почта" ref={register} error={errors.email?.message} />
            <InputText name="password" label="Пароль" ref={register} type="password" error={errors.password?.message} />
            <S.SubmitButton as={Button} label="Войти в систему" type="submit" />
        </S.Form>
    );
};
