import Icon from 'components/base/icon';
import { IconType } from 'components/base/icon/types';
import Link from 'components/base/Link';
import Typography from 'components/base/Typography';
import AuthLayout from 'components/layouts/AuthLayout';
import React from 'react';
import { Routes } from 'router/routes';
import { LoginForm } from './components/login-form';
import styled from 'styled-components/macro';
import { Colors } from 'styles/colors';
import { useLinkLogin } from './hooks/use-link-login';

const HelpLink = styled.a`
    color: ${Colors.Orange};
    font-size: 14px;
    text-align: center;
    display: block;
    margin-top: 15px;
`;

export const LoginPage: React.FC = () => {
    useLinkLogin();

    const renderHeaderContent = () => <Icon type={IconType.Logo} />;

    const renderFooterContent = () => (
        <React.Fragment>
            <Typography type="p" size={14}>
                Ещё нет аккаунта?
            </Typography>
            <Link to={Routes.Register} icon={IconType.Arrow} iconSide="right">
                Зарегистрироваться
            </Link>
        </React.Fragment>
    );

    return (
        <AuthLayout headerContent={renderHeaderContent()} footerContent={renderFooterContent()}>
            <LoginForm />
            <Link to={Routes.ResetPassword}>Забыли пароль?</Link>
            <HelpLink href="http://medapphelp.tilda.ws" rel="noopener noreferrer" target="_blank">
                Как пользоваться?
            </HelpLink>
        </AuthLayout>
    );
};
