import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { LogInAction } from 'store/auth/auth.actions';

export function useLinkLogin() {
    const history = useHistory();
    const dispatch = useDispatch();

    useEffect(() => {
        const urlPrams = new URLSearchParams(window.location.search);
        const link = urlPrams.get('link');

        if (link) {
            dispatch({ ...new LogInAction('link', link) });
            urlPrams.delete('link');
            history.replace({
                search: urlPrams.toString(),
            });
        }
    }, [dispatch, history]);
}
