import { Button } from 'components/base/button';
import { InputText } from 'components/base/input';
import * as React from 'react';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers';
import { useForm } from 'react-hook-form';
import * as S from './reset-password-form.styles';
import { useDispatch } from 'react-redux';
import { SendCodeAction } from 'store/auth/auth.actions';

type FormData = {
    email: string;
};

const schema = yup.object().shape({
    email: yup.string().required('Поле почты является обязательным').email('Введите почту корректно'),
});

const ResetForm: React.FC = () => {
    const dispatch = useDispatch();
    const { register, handleSubmit, errors, reset } = useForm<FormData>({
        resolver: yupResolver(schema),
    });

    const onSubmit = handleSubmit((data) => {
        dispatch({ ...new SendCodeAction({ email: data.email, action: 'U' }) });
        reset();
    });
    return (
        <S.Form onSubmit={onSubmit}>
            <InputText name="email" label="Ваша почта" isClear={true} ref={register} error={errors.email?.message} />
            <S.SubmitButton as={Button} label="Восстановить пароль" type="submit" />
        </S.Form>
    );
};

export default ResetForm;
