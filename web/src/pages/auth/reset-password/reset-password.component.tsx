import { IconType } from 'components/base/icon/types';
import Link from 'components/base/Link';
import Typography from 'components/base/Typography';
import AuthLayout from 'components/layouts/AuthLayout';
import React from 'react';
import { Routes } from 'router/routes';
import { Colors } from 'styles/colors';
import ResetForm from './reset-password-form/reset-password-form.component';

export const ResetPasswordPage: React.FC = () => {
    const renderHeaderContent = () => (
        <React.Fragment>
            <Typography color={Colors.White} type="h1" weight="bold" size={28}>
                Восстановление
            </Typography>
            <Typography color={Colors.White} type="p" weight="regular" size={14}>
                Введите почту и мы отправим Вам код для восстановления пароля
            </Typography>
        </React.Fragment>
    );

    const renderFooterContent = () => (
        <React.Fragment>
            <Link rotateIcon={180} to={Routes.Login} icon={IconType.Arrow} iconSide="left">
                Я вспомнил пароль
            </Link>
        </React.Fragment>
    );
    return (
        <AuthLayout headerContent={renderHeaderContent()} footerContent={renderFooterContent()}>
            <ResetForm />
        </AuthLayout>
    );
};
