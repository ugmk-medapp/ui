import React from 'react';
import AuthLayout from 'components/layouts/AuthLayout';
import Icon from 'components/base/icon';
import { IconType } from 'components/base/icon/types';
import Link from 'components/base/Link';
import { Routes } from 'router/routes';
import { RegisterForm } from './sign-up-form';

export const SignUpPage: React.FC = () => {
    const renderHeaderContent = () => <Icon type={IconType.Logo} />;

    const renderFooterContent = () => (
        <React.Fragment>
            <Link rotateIcon={180} to={Routes.Login} icon={IconType.Arrow} iconSide="left">
                Войти в аккаунт
            </Link>
        </React.Fragment>
    );
    return (
        <AuthLayout headerContent={renderHeaderContent()} footerContent={renderFooterContent()}>
            <RegisterForm />
        </AuthLayout>
    );
};
