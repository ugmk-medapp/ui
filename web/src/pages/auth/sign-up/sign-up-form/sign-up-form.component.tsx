import { InputText } from 'components/base/input';
import React from 'react';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers';
import { useForm } from 'react-hook-form';
import { Button } from 'components/base/button';
import { useDispatch, useSelector } from 'react-redux';
import { RegisterAction } from 'store/auth/auth.actions';
import { RootState } from 'store/root.reducer';
import Typography from 'components/base/Typography';
import { Colors } from 'styles/colors';
import { RegisterStatus } from '@ugmk/api';
import * as S from './sign-up-form.styles';
type FormData = {
    email: string;
    password: string;
    confirmPassword: string;
};

const schema = yup.object().shape({
    email: yup.string().required('Поле почты является обязательным').email('Введите почту корректно'),
    password: yup.string().required('Поле пароля является обязательным'),
    confirmPassword: yup.string().oneOf([yup.ref('password')], 'Пароли не совпадают'),
});

interface Props {}

export const RegisterForm: React.FC<Props> = () => {
    const dispatch = useDispatch();
    const registerStatus = useSelector((state: RootState) => state.auth.registerStatus);
    const registerError = useSelector((state: RootState) => state.auth.registerError);
    const { register, handleSubmit, errors, reset } = useForm<FormData>({
        resolver: yupResolver(schema),
    });

    const onSubmit = handleSubmit((data) => {
        dispatch({
            ...new RegisterAction(data.email, data.password),
        });
        reset();
    });

    return (
        <S.Form onSubmit={onSubmit}>
            <InputText name="email" label="Ваша почта" isClear={true} ref={register} error={errors.email?.message} />
            <InputText name="password" label="Пароль" ref={register} type="password" error={errors.password?.message} />
            <InputText
                name="confirmPassword"
                label="Пароль повторно"
                type="password"
                ref={register}
                error={errors.confirmPassword?.message}
            />
            {registerError && (
                <Typography type="span" size={11} color={Colors.Red} marginBottom={5}>
                    {registerStatus === RegisterStatus.Exists
                        ? 'Пользователь с таким email уже существует'
                        : 'Ошибка сервера'}
                </Typography>
            )}
            <S.SubmitButton as={Button} label="Зарегистрироваться" type="submit" />
        </S.Form>
    );
};
