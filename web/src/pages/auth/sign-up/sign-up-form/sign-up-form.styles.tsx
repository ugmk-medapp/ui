import styled from 'styled-components/macro';

export const SubmitButton = styled.button``;

export const Form = styled.form`
    display: flex;
    flex-direction: column;
    & > * {
        margin-bottom: 20px;
    }

    ${SubmitButton} {
        margin-top: 10px;
    }
`;
