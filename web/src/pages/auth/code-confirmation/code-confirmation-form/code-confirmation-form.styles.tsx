import styled from 'styled-components';
import { Colors } from 'styles/colors';

export const RetryCodeButton = styled.button``;

export const Container = styled.div`
    display: flex;
    flex-direction: column;

    ${RetryCodeButton} {
        margin-bottom: 23px;
    }
`;

export const NumberInput = styled.div`
    width: 60px;
    height: 68px;
    background-color: ${Colors.DarkWhite};
    border-radius: 15px;
    color: ${Colors.LightBlack};
    font-size: 28px;
    font-weight: bold;
    text-align: center;
    line-height: 68px;
`;

export const NumbersContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    width: 100%;
    margin-bottom: 47px;
`;
