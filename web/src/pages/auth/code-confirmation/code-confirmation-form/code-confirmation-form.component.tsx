import React, { useCallback, useEffect, useRef, useState } from 'react';
import * as S from './code-confirmation-form.styles';
import Typography from 'components/base/Typography';
import { Colors } from 'styles/colors';
import { useDispatch, useSelector } from 'react-redux';
import { ConfirmCodeAction, GetWaitingTimeCodeAction } from 'store/auth/auth.actions';
import { SendCodeAction } from 'store/auth/auth.actions';
import { Routes } from 'router/routes';
import { RootState } from 'store/root.reducer';
import { Button } from 'components/base/button';

interface Props {
    email: string | null;
}

export const CodeConfirmationForm: React.FC<Props> = ({ email }) => {
    const dispatch = useDispatch();
    const startExpiredCodeTime = useSelector((state: RootState) => state.auth.waitingTimeCode);
    const expiredCodeTime = useExpiredCodeTime(startExpiredCodeTime);
    const confirmError = useSelector((state: RootState) => state.auth.confirmCodeError);
    const [code, setCode] = useState<string>('');
    const inputRef = useRef<HTMLInputElement | null>(null);
    const actionRef = useRef<'C' | 'U'>('C');

    useEffect(() => {
        if (email) {
            dispatch({ ...new GetWaitingTimeCodeAction(email) });
        }
    }, [dispatch, email]);

    useEffect(() => {
        if (window.location.pathname === Routes.ResetPasswordConfirm) {
            actionRef.current = 'U';
        }
    }, []);

    useEffect(() => {
        inputRef.current?.focus();
    }, [inputRef]);

    const onInputChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;

        if (!!+value && value.length <= 4) {
            setCode(value);

            if (value.length === 4 && email) {
                dispatch({ ...new ConfirmCodeAction({ email, code: value, action: actionRef.current }) });
            }
        } else if (value === '') {
            setCode('');
        }
    };

    const renderDigitCodeInput = useCallback(() => {
        return [1, 2, 3, 4].map((num) => {
            return (
                <S.NumberInput key={num} onClick={() => inputRef.current?.focus()}>
                    {code && code.toString()[num - 1] ? code.toString()[num - 1] : ''}
                </S.NumberInput>
            );
        });
    }, [code, inputRef]);

    return (
        <S.Container>
            <S.NumbersContainer>
                <input
                    type="number"
                    value={code}
                    onChange={onInputChangeHandler}
                    ref={inputRef}
                    style={{ position: 'absolute', opacity: 0 }}
                />
                {renderDigitCodeInput()}
            </S.NumbersContainer>
            {confirmError && (
                <Typography type="span" color={Colors.Red} size={11}>
                    {confirmError}
                </Typography>
            )}
            {expiredCodeTime !== undefined && expiredCodeTime > 0 && (
                <Typography type="p" size={14} color={Colors.DarkGray}>
                    Отправить код повторно (00:{expiredCodeTime})
                </Typography>
            )}
            {expiredCodeTime === undefined ||
                (expiredCodeTime === 0 && (
                    <S.RetryCodeButton
                        as={Button}
                        label="Отправить код повторно"
                        onClick={() => {
                            if (email) {
                                dispatch({ ...new SendCodeAction({ email, action: actionRef.current }) });
                                dispatch({ ...new GetWaitingTimeCodeAction(email) });
                            }
                        }}
                    />
                ))}
        </S.Container>
    );
};

function useExpiredCodeTime(startTime: number | undefined) {
    const [expiredCodeTime, setExpiredCodeTime] = useState<number | undefined>();

    useEffect(() => {
        setExpiredCodeTime(startTime);
    }, [startTime]);

    useEffect(() => {
        const interval = setInterval(() => {
            setExpiredCodeTime((prevState) => {
                if (prevState !== undefined) {
                    if (prevState === 0) {
                        clearInterval(interval);
                        return prevState;
                    }
                    return prevState - 1;
                } else {
                    clearInterval(interval);
                    return prevState;
                }
            });
        }, 1000);

        return () => clearInterval(interval);
    }, [startTime]);

    return expiredCodeTime;
}
