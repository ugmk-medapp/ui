import { IconType } from 'components/base/icon/types';
import Link from 'components/base/Link';
import Typography from 'components/base/Typography';
import AuthLayout from 'components/layouts/AuthLayout';
import { replace } from 'connected-react-router';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Routes } from 'router/routes';
import { CONFIRM_EMAIL } from 'state/constants';
import { Colors } from 'styles/colors';
import { CodeConfirmationForm as Form } from './code-confirmation-form';

const CodeConfirmPage: React.FC = () => {
    const dispatch = useDispatch();
    const [email, setEmail] = useState<string | null>(null);

    useEffect(() => {
        const confirmEmail = window.localStorage.getItem(CONFIRM_EMAIL);

        if (confirmEmail) {
            setEmail(confirmEmail);
        } else {
            dispatch(replace(Routes.Register));
        }
    }, [dispatch, setEmail]);

    const renderHeaderContent = () => (
        <React.Fragment>
            <Typography color={Colors.White} type="h1" weight="bold" size={28}>
                Введите код
            </Typography>
            <Typography color={Colors.White} type="p" weight="regular" size={14}>
                Мы отправили Вам код для восстановления доступа на почту {'***' + email?.slice(3)}
            </Typography>
        </React.Fragment>
    );

    const renderFooterContent = () => (
        <Link rotateIcon={180} to={Routes.Login} icon={IconType.Arrow} iconSide="left">
            Я вспомнил пароль
        </Link>
    );

    return (
        <AuthLayout headerContent={renderHeaderContent()} footerContent={renderFooterContent()}>
            <Form email={email} />
        </AuthLayout>
    );
};

export default CodeConfirmPage;
