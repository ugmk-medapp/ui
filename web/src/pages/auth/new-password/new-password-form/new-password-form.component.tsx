import { yupResolver } from '@hookform/resolvers';
import { Button } from 'components/base/button';
import { InputText } from 'components/base/input';
import * as S from './new-password-form.styles';
import React from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { RestorePasswordAction } from 'store/auth/auth.actions';
import * as yup from 'yup';

type FormData = {
    password: string;
    confirmPassword: string;
};

const schema = yup.object().shape({
    password: yup.string().required('Поле пароля является обязательным'),
    confirmPassword: yup.string().oneOf([yup.ref('password')], 'Пароли не совпадают'),
});

interface Props {
    resetId: string | null;
}

export const NewPasswordForm: React.FC<Props> = ({ resetId }) => {
    const dispatch = useDispatch();
    const { register, handleSubmit, errors, reset } = useForm<FormData>({
        resolver: yupResolver(schema),
    });

    const onSubmit = handleSubmit((data) => {
        console.log(data, resetId);
        if (resetId) {
            dispatch({ ...new RestorePasswordAction(resetId, data.password) });
        }
        reset();
    });
    return (
        <S.Form onSubmit={onSubmit}>
            <InputText name="password" label="Пароль" ref={register} type="password" error={errors.password?.message} />
            <InputText
                name="confirmPassword"
                label="Пароль повторно"
                type="password"
                ref={register}
                error={errors.confirmPassword?.message}
            />
            <S.SubmitButton as={Button} label="Изменить пароль" type="submit" />
        </S.Form>
    );
};
