import React, { useEffect, useState } from 'react';
import AuthLayout from 'components/layouts/AuthLayout';
import { IconType } from 'components/base/icon/types';
import Link from 'components/base/Link';
import { Routes } from 'router/routes';
import Typography from 'components/base/Typography';
import { Colors } from 'styles/colors';
import { useDispatch } from 'react-redux';
import { RESET_PASSWORD_ID } from 'state/constants';
import { replace } from 'connected-react-router';
import { NewPasswordForm } from './new-password-form';

export const NewPasswordPage: React.FC = () => {
    const dispatch = useDispatch();
    const [resetId, setResetId] = useState<string | null>(null);

    useEffect(() => {
        const resetPasswordId = window.localStorage.getItem(RESET_PASSWORD_ID);

        if (resetPasswordId) {
            setResetId(resetPasswordId);
        } else {
            dispatch(replace(Routes.ResetPassword));
        }
    }, [dispatch, setResetId]);

    const renderHeaderContent = () => (
        <React.Fragment>
            <Typography color={Colors.White} type="h1" weight="bold" size={28}>
                Новый пароль
            </Typography>
            <Typography color={Colors.White} type="p" weight="regular" size={14}>
                Новый пароль не должен совпадать с ранее созданным
            </Typography>
        </React.Fragment>
    );

    const renderFooterContent = () => (
        <React.Fragment>
            <Link rotateIcon={180} to={Routes.Login} icon={IconType.Arrow} iconSide="left">
                Я вспомнил пароль
            </Link>
        </React.Fragment>
    );
    return (
        <AuthLayout headerContent={renderHeaderContent()} footerContent={renderFooterContent()}>
            <NewPasswordForm resetId={resetId} />
        </AuthLayout>
    );
};
