import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Routes } from 'router/routes';
import CodeConfirmPage from './code-confirmation';
import { LoginPage } from './login';
import { NewPasswordPage } from './new-password';
import { ResetPasswordPage } from './reset-password';
import { SignUpPage } from './sign-up';

const AuthPageSwitcher: React.FC = () => {
    return (
        <Switch>
            <Redirect exact from={'/'} to={Routes.Login} />
            <Route exact path={Routes.Login} component={LoginPage} />
            <Route exact path={Routes.RegisterConfirm} component={CodeConfirmPage} />
            <Route exact path={Routes.Register} component={SignUpPage} />
            <Route exact path={Routes.ResetPasswordConfirm} component={CodeConfirmPage} />
            <Route exact path={Routes.ResetPassword} component={ResetPasswordPage} />
            <Route exact path={Routes.NewPassword} component={NewPasswordPage} />
            <Route component={() => <div>Not Found</div>} />
        </Switch>
    );
};

export default AuthPageSwitcher;
