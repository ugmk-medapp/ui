import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import RootRouter from './router';
import { Provider } from 'react-redux';
import './assets/styles/init-fonts.css';
import store, { history } from './store';
import { GlobalStyles } from './styles/global-styles';
import AppStateProvider from 'state';
import InfoModal from 'components/partial/info-modal';
import { AxiosError, initAxios } from '@ugmk/api';
import { ConnectedRouter } from 'connected-react-router';
import { AuthErrorAction } from 'store/auth/auth.actions';

initAxios((window as any)._env_.API_DEV_LOCAL_URL, (error: AxiosError) => {
    if (401 === error.response?.status) {
        store.dispatch({ ...new AuthErrorAction() });
    }
});

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <GlobalStyles />
            <AppStateProvider>
                <ConnectedRouter history={history}>
                    <RootRouter />
                    <InfoModal />
                </ConnectedRouter>
            </AppStateProvider>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
