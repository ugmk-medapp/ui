export default interface InfoModalModel {
    title?: string;
    contentMessage: string;
    buttonText: string;
    onClose?: () => void;
}