// Это еще одна вещь за которую должно быть стыдно. Но не мне))
/**
 * @param Schedule - по расписанию
 * @param Service - на оказание
 */
export enum AppointmentPageType {
    Schedule = 'schedule',
    Service = 'service',
}
