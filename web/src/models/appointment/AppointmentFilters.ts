import { AppointmentFilterType, IAppointmentType, ServiceType } from '@ugmk/api';

export default interface AppointmentFilters {
    type?: IAppointmentType;
    serviceType: ServiceType;
    dateFilter: string;
    page: number;
    searchName?: string;
    filterType: AppointmentFilterType;
}
