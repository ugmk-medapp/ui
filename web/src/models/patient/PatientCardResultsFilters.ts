import { ServiceType } from '@ugmk/api';

export default interface PatientCardResultsFilters {
    page: number;
    searchString?: string;
    patientId?: string;
    serviceType: ServiceType;
}
