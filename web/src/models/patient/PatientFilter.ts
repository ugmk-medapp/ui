export default interface PatientFilters {
    page: number;
    searchName?: string;
}