import { RootState } from "store/root.reducer";
import { AuthState } from "./auth.reducers";
import { createSelector } from 'reselect';

export const authSelector = (state: RootState): AuthState => state.auth;

export const currentStaffUnitSelector = createSelector([authSelector], (authState) => authState.currentStaffUnit);