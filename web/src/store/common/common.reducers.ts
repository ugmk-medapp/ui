import { combineReducers } from "redux";
import { infoModalReducer, InfoModalState } from "./info-modal/info-modal.reducers";

export interface CommonState {
    infoModal: InfoModalState;
}

export const commonReducer = combineReducers<CommonState>({
    infoModal: infoModalReducer
});