import React from 'react';
import { LoaderContainer, LoaderSpinner } from './styles';
import { ReactComponent as LoaderIcon } from 'assets/icons/ic-loader.svg';

interface Props {
    fullScreen?: boolean;
}

const Loader: React.FC<Props> = ({ fullScreen }) => {
    return (
        <LoaderContainer active={fullScreen}>
            <LoaderSpinner>
                <LoaderIcon />
            </LoaderSpinner>
        </LoaderContainer>
    );
};

export default Loader;
