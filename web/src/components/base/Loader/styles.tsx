import styled, { keyframes } from 'styled-components';

const rotate = keyframes`
    100% {
        transform: rotate(360deg);
    }
`;

const dash = keyframes`
    0% {
        stroke-dasharray: 1, 200;
        stroke-dashoffset: 0;
    }
    50% {
        stroke-dasharray: 89, 200;
        stroke-dashoffset: -35px;
    }
    100% {
        stroke-dasharray: 89, 200;
        stroke-dashoffset: -124px;
    }
`;

export const LoaderContainer = styled.div<{ active?: boolean }>`
    width: 100%;
    height: ${({ active }) => (active ? '100vh' : 'initial')};
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const LoaderSpinner = styled.div`
    position: relative;
    margin: auto;
    width: 66px;
    height: 66px;

    svg {
        animation: ${rotate} 2s linear infinite;
        height: 100%;
        transform-origin: center center;
        width: 100%;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
    }

    circle {
        stroke-dasharray: 1, 200;
        stroke-dashoffset: 0;
        animation: ${dash} 1.5s ease-in-out infinite;
        stroke-linecap: round;
        stroke: #f4662a;
    }
`;
