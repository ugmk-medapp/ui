import * as React from 'react';
import Icon from '../icon';
import { IconType } from '../icon/types';
import { IconSide, InsideSpaceDivider, LinkContainer } from './styles';

interface Props {
    icon?: IconType;
    iconSide?: IconSide;
    rotateIcon?: number;
    to: string;
}
/**
 *
 * @param icon `IconType` (by default on left side)
 * @param iconSide `left | right`
 * @param rotateIcon `number` rotate angle of icon in degrees
 * @param to `string`
 * @returns
 */
const Link: React.FC<Props> = (props) => {
    return (
        <LinkContainer rotate={props.rotateIcon || 0} side={props.iconSide} to={props.to}>
            {!!props.icon && (
                <>
                    <Icon type={props.icon} />
                    <InsideSpaceDivider />
                </>
            )}

            {props.children}
        </LinkContainer>
    );
};

Link.defaultProps = {
    iconSide: 'left',
    rotateIcon: 0,
};

export default Link;
