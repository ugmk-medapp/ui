import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Colors } from 'styles/colors';

export type IconSide = 'left' | 'right';

export const LinkContainer = styled(Link)<{ side?: IconSide; rotate: number }>`
    display: flex;
    flex-direction: ${({ side }) => (side === 'left' ? 'row' : 'row-reverse')};
    justify-content: center;
    align-items: center;
    color: ${Colors.Orange};
    text-decoration: none;
    user-select: none;
    -webkit-tap-highlight-color: transparent;
    font-size: 14px;
    font-weight: 500;

    svg {
        width: 24px;
        height: 24px;
        transform: rotate(${({ rotate }) => `${rotate}deg`});
    }
`;

export const InsideSpaceDivider = styled.div`
    width: 10px;
`;
