import React, { useState, useEffect } from 'react';
import * as Styles from './styles';

interface Props {
    label?: string;
    defaultValue?: boolean;
    disabled?: boolean;
    onChange: (isActive: boolean) => void;
}

const Switcher: React.FC<Props> = (props) => {
    const { label, defaultValue, disabled = false, onChange } = props;

    useEffect(() => {
        if (defaultValue) {
            onChange(defaultValue);
        }
    }, [defaultValue, onChange]);

    const [isActive, changeActive] = useState<boolean>(defaultValue !== undefined ? defaultValue : false);
    return (
        <Styles.SwitcherContainer
            onClick={() => {
                if (disabled) return;
                onChange(!isActive);
                changeActive((prevState) => !prevState);
            }}
        >
            <Styles.Label>{label}</Styles.Label>
            <Styles.Controller active={isActive} disabled={disabled}>
                <Styles.SwitchButton />
            </Styles.Controller>
        </Styles.SwitcherContainer>
    );
};

export default Switcher;
