import useOutsideClickHandler from 'hooks/useOutsideClickHandler';
import debounce from 'lodash.debounce';
import React, { useEffect, useRef, useState } from 'react';
import { IconType } from '../icon/types';
import { InputText } from '../input';
import * as S from './autocomplete.styles';

interface Props<T> {
    items: T[];
    defaultValue?: T;
    label: string;
    icon?: IconType;
    labelSelector: (item: T) => string;
    onSelect: (item: T) => void;
    search: (value: string) => void;
}

export function Autocomplete<T>(props: Props<T>) {
    const { defaultValue, items, label, icon, onSelect, search, labelSelector } = props;
    const autocompleteRef = useRef<HTMLDivElement | null>(null);
    const inputRef = useRef<HTMLInputElement | null>(null);
    useOutsideClickHandler(autocompleteRef, () => openDropdown(false));

    const [isOpened, openDropdown] = useState(false);
    const [inputValue, setInputValue] = useState<string>('');
    const [selectedItem, setSelectedItem] = useState<T | null>(null);

    useEffect(() => {
        const input = inputRef.current;
        if (isOpened && input) {
            const searchString = input.value;
            search(searchString);
        }
    }, [isOpened, search]);

    useEffect(() => {
        if (!defaultValue || !inputRef.current) return;
        const inputText = inputRef.current;

        setSelectedItem(defaultValue);
        inputText.value = labelSelector(defaultValue);
        setInputValue(labelSelector(defaultValue));
    }, [defaultValue, inputRef, labelSelector]);

    const selectItem = (item: T) => {
        if (!inputRef.current) return;
        const inputText = inputRef.current;

        setSelectedItem(item);
        setInputValue(labelSelector(item));
        onSelect(item);
        openDropdown(false);
        inputText.value = labelSelector(item);
    };

    const onInputChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const searchString = event.target.value;
        setInputValue(searchString);
        if (selectedItem && searchString !== labelSelector(selectedItem)) {
            setSelectedItem(null);
        }
        if (searchString.length > 0 && !isOpened) {
            openDropdown(true);
        }
        debounce(() => search(searchString), 150)();
    };

    return (
        <S.Container active={isOpened} ref={autocompleteRef}>
            <InputText
                label={label}
                ref={inputRef}
                onChange={onInputChangeHandler}
                onClick={() => openDropdown(!isOpened)}
                onClearHandle={() => setInputValue('')}
                type="text"
                defaultValue={inputValue}
                icon={icon}
                isClear={true}
            />
            <S.List active={isOpened}>
                {items.map((item: T, ind: number) => (
                    <S.ListItem key={ind} onClick={() => selectItem(item)}>
                        {labelSelector(item)}
                    </S.ListItem>
                ))}
            </S.List>
        </S.Container>
    );
}
