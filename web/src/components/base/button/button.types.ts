type Theme = 'filled' | 'ghost';
type Size = 'big' | 'medium' | 'small';
export type Side = 'right' | 'left';

export interface ButtonContainerProps {
    theme?: Theme;
    disabled?: boolean;
    size?: Size;
    side?: Side;
}
