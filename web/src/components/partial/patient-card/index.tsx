import { Button } from 'components/base/button';
import { IconType } from 'components/base/icon/types';
import Typography from 'components/base/Typography';
import { push } from 'connected-react-router';
import React from 'react';
import { useDispatch } from 'react-redux';
import { generatePath } from 'react-router-dom';
import { Routes } from 'router/routes';
import { Colors } from 'styles/colors';
import { PatientCardContainer } from './styles';

interface Props {
    name: string;
    surname: string;
    patronymic: string;
    birthYear: string | null;
    patientId: string;
}

const PatientCard: React.FC<Props> = (props) => {
    const dispatch = useDispatch();
    const patientName = `${props.surname} ${props.name} ${props.patronymic[0]}.`;

    return (
        <PatientCardContainer>
            <Typography type="h3" color={Colors.Orange} textAlign="left" marginBottom={6} weight="medium">
                {patientName}
            </Typography>
            <Typography type="span" size={14} textAlign="left" marginBottom={20}>
                Год рождения — {props.birthYear}
            </Typography>
            <Button
                label="Карта пациента"
                size="medium"
                theme="ghost"
                side="left"
                icon={IconType.PatientCard}
                onClick={() => {
                    dispatch(push(generatePath(Routes.PatientCard, { id: props.patientId })));
                }}
            />
        </PatientCardContainer>
    );
};

export default PatientCard;
