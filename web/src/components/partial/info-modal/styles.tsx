import styled from 'styled-components';

export const InfoModalContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
`;
