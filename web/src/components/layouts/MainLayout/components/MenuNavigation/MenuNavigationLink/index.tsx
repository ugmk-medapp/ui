import Icon from 'components/base/icon';
import { IconType } from 'components/base/icon/types';
import Typography from 'components/base/Typography';
import React from 'react';
import { generatePath } from 'react-router';
import { Routes } from 'router/routes';
import { MenuNavigationLinkContainer } from './styles';

interface Props {
    icon: IconType;
    label: string;
    to: Routes;
    active: boolean;
}

const MenuNavigationLink: React.FC<Props> = (props) => {
    return (
        <MenuNavigationLinkContainer to={generatePath(props.to)} active={+props.active}>
            <Icon type={props.icon} />
            <Typography type="span" size={11} weight="regular" marginBottom={0}>
                {props.label}
            </Typography>
        </MenuNavigationLinkContainer>
    );
};

export default MenuNavigationLink;
