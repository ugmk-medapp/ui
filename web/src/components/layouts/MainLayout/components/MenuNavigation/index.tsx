import { IconType } from 'components/base/icon/types';
import React from 'react';
import { matchPath, useHistory } from 'react-router-dom';
import { Routes } from 'router/routes';
import MenuNavigationLink from './MenuNavigationLink';
import { MenuNavigationContainer } from './styles';

const MenuNavigation: React.FC = () => {
    const history = useHistory();

    const isLinkActive = (menuLink: IMenuLink) => {
        let isActive = !!matchPath(menuLink.route, {
            path: history.location.pathname,
        })?.isExact;

        if (menuLink.childRoutes && menuLink.childRoutes.length > 0) {
            menuLink.childRoutes.forEach((cr) => {
                if (
                    !!matchPath(history.location.pathname, {
                        path: cr,
                        exact: true,
                        strict: false,
                    })?.isExact
                ) {
                    isActive = true;
                    return;
                }
            });
        }

        return isActive;
    };

    return (
        <MenuNavigationContainer>
            {MenuLinks.map((menuLink) => {
                return (
                    <MenuNavigationLink
                        key={menuLink.route}
                        to={menuLink.route}
                        icon={menuLink.icon}
                        label={menuLink.title}
                        active={isLinkActive(menuLink)}
                    />
                );
            })}
        </MenuNavigationContainer>
    );
};

interface IMenuLink {
    title: string;
    icon: IconType;
    route: Routes;
    childRoutes?: Routes[];
}

const MenuLinks: IMenuLink[] = [
    {
        title: 'На оказание',
        icon: IconType.Heart,
        route: Routes.Service,
        childRoutes: [Routes.AppointmentService],
    },
    {
        title: 'По расписанию',
        icon: IconType.BigCalendar,
        route: Routes.Schedule,
        childRoutes: [Routes.AppointmentSchedule],
    },
    {
        title: 'Карты пациентов',
        icon: IconType.People,
        route: Routes.PatientCardList,
        childRoutes: [Routes.PatientCard],
    },
];

export default MenuNavigation;
