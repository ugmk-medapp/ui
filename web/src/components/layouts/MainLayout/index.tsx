import Header from './components/Header';
import MenuNavigation from './components/MenuNavigation';
import HelpLink from './components/help-link/help-link.component';
import React from 'react';
import { Content, MainLayoutContainer } from './styles';
import MainLayoutProvider, { useMainLayoutState } from './state/MainLayoutProvider';

const Container: React.FC = ({ children }) => {
    const { headerActive } = useMainLayoutState();

    return (
        <MainLayoutContainer isHeaderActive={headerActive}>
            <Content>{children}</Content>
        </MainLayoutContainer>
    );
};

Container.defaultProps = {
    withBottomNavigation: true,
};

const MainLayout: React.FC = ({ children }) => {
    return (
        <MainLayoutProvider>
            <Container>{children}</Container>
        </MainLayoutProvider>
    );
};

export default Object.assign(MainLayout, { Header, MenuNavigation, HelpLink });
