export enum Routes {
    Service = '/service',
    Login = '/login',
    Register = '/register',
    RegisterConfirm = '/register/confirm',
    ResetPassword = '/password/reset',
    ResetPasswordConfirm = '/password/reset/confirm',
    NewPassword = '/password/new',
    Schedule = '/schedule',
    PatientCardList = '/patients/cards/',
    PatientCard = '/patients/cards/:id?',
    AppointmentSchedule = '/appointment/schedule/:id',
    AppointmentService = '/appointment/service/:id',
}
