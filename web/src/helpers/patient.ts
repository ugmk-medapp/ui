import { IPatient } from '@ugmk/api';

export function getPatientCardName(patient: IPatient) {
    let name: string = `${patient.surname} ${patient.name}`;
    return patient.patronymic ? `${name} ${patient.patronymic[0]}.` : name;
}
