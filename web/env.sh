#!/bin/bash

# Recreate config file
rm -rf ./env-config.js
touch ./env-config.js

# Add assignment
echo "window._env_ = {" >> ./env-config.js
  # Append configuration property to JS file
  echo "  API_DEV_LOCAL_URL: \"$API_URL\"," >> ./env-config.js

echo "}" >> ./env-config.js

exit 0
